import 'quasar/dist/quasar.css';
import '@quasar/extras/roboto-font/roboto-font.css';
import '@quasar/extras/material-icons/material-icons.css';
import '@quasar/extras/material-icons-outlined/material-icons-outlined.css';
import '@quasar/extras/material-icons-round/material-icons-round.css';
import '@quasar/extras/material-icons-sharp/material-icons-sharp.css';
import '@quasar/extras/mdi-v7/mdi-v7.css';
import { Notify, Dark } from 'quasar';

// To be used on app.use(Quasar, { ... })
export default {
  config: {
    Dark,
    brand: {
      primary: '#1976d2',
      secondary: '#26A69A',
      accent: '#9C27B0',

      dark: '#121212',
      'dark-page': '#000000',

      positive: '#21BA45',
      negative: '#C10015',
      info: '#31CCEC',
      warning: '#F2C037',
      // dark: {
      //   primary: '#000000',
      // },
    },
  },
  plugins: {
    Notify,
  },
};
