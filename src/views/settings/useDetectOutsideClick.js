import { onBeforeUnmount, onMounted } from 'vue';

export default function useDetectOutsideClick(component, callback) {
  if (!component) return null;
  const listener = (event) => {
    if (event.target !== component.value && event.composedPath().includes(component.value)) {
      return null;
    }
    if (typeof callback === 'function') {
      callback();
    }
    return null;
  };
  onMounted(() => { window.addEventListener('click', listener); });
  onBeforeUnmount(() => { window.removeEventListener('click', listener); });

  return { listener };
}
