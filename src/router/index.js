import { createRouter, createWebHistory } from 'vue-router';
import store from '../store';

// Function for charge fast views.
function lazyLoadViews(view) {
  return () => import(`@/views/${view}.vue`);
}
// Function for charge fast views.
function lazyLoadLayeouts(view) {
  return () => import(`@/layouts/${view}.vue`);
}
// Function for charge fast componet.
// function lazyLoadComponents(view) {
//   return () => import(`@/components/${view}.vue`);
// }
// List rutes of progrmas
const routes = [
  {
    path: '/',
    name: 'main',
    component: lazyLoadViews('main/MainView'),
  },
  {
    path: '/about',
    name: 'about',
    component: lazyLoadViews('AboutView'),
  },
  {
    path: '/',
    name: 'accounts',
    component: lazyLoadLayeouts('AccountLayout'),
    children: [
      {
        name: 'login',
        path: 'login',
        component: lazyLoadViews('accounts/LoginView'),
      },
      {
        name: 'register',
        path: 'register',
        component: lazyLoadViews('accounts/RegisterView'),
      },
      {
        name: 'resetpassword',
        path: 'resetpassword',
        component: lazyLoadViews('accounts/ResetPasswordView'),
      },
    ],
  },
  {
    path: '/reports',
    name: 'reports',
    component: lazyLoadLayeouts('GeneralLayout'),
    children: [
      {
        name: 'routeReport',
        path: 'route',
        component: lazyLoadViews('reports/RouteView'),
      },
      {
        name: 'eventReport',
        path: 'event',
        component: lazyLoadViews('reports/EventView'),
      },
    ],
  },
  {
    path: '/settings',
    name: 'settings',
    component: lazyLoadLayeouts('GeneralLayout'),
    children: [
      {
        name: 'userSettings',
        path: 'user',
        component: lazyLoadViews('settings/AccountView'),
      },
      {
        name: 'userIdSettings',
        path: 'user/:id',
        props: true,
        component: lazyLoadViews('settings/AccountView'),
      },
      {
        name: 'preferenceSettings',
        path: 'preferences',
        component: lazyLoadViews('settings/PreferenceView'),
      },
    ],
  },
  {
    path: '/:catchAll(.*)',
    component: lazyLoadViews('NotFoundPage'),
  },
];
// Create route for programa
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});
// Valid if user is auth
router.beforeEach(async (to, from, next) => {
  const initialized = store.getters.user;
  if (!initialized) {
    try {
      const response = await fetch('/api/session');
      if (response.ok) {
        store.commit('updateUser', await response.json());
        next();
      } else if (!response.ok) {
        if (to.name === 'login' || to.name === 'register' || to.name === 'resetpassword') next();
        else next({ name: 'login' });
      }
    } catch (error) {
      throw Error('error');
    }
  } else {
    next();
  }
});

export default router;
