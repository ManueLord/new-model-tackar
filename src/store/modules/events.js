// Elements
const state = {
  events: [],
};
// Call elemnets on this module to another components
const getters = {
  events: (state) => state.events,
};
// Acctions
const mutations = {
  // Add new event
  eventsAdd(state, payload) {
    state.events.unshift(...payload);
  },
  // Delete event spscific
  eventsDelete(state, payload) {
    state.events = state.events.filter((item) => item.id !== payload.id);
  },
  // Delete all events
  eventsDeleteAll(state) {
    state.events = [];
  },
};

export default { state, getters, mutations };
