// Elements
const state = {
  devicesItems: {},
  deviceSelectedId: null,
  deviceSelectedIds: [],
};
// Call elemnets on this module to another components
const getters = {
  devicesItems: (state) => state.devicesItems,
  deviceSelectedId: (state) => state.deviceSelectedId,
  deviceSelectedIds: (state) => state.deviceSelectedIds,
};
// Acctions
const mutations = {
  // Select device
  devicesSelectId(state, payload) {
    state.deviceSelectedId = payload;
    state.deviceSelectedIds = state.deviceSelectedId ? [state.deviceSelectedId] : [];
  },
  // Select more device
  devicesSelectIds(state, payload) {
    state.deviceSelectedIds = payload;
    [state.deviceSelectedId] = state.deviceSelectedIds;
  },
  // Delete device
  devicesRemove(state, payload) {
    delete state.devicesItems[payload];
  },
  // Update date of the devices
  devicesUpdate(state, payload) {
    payload.forEach((item) => { state.devicesItems[item.id] = item; });
  },
  // Refresh all list devices
  devicesRefresh(state, payload) {
    state.devicesItems = {};
    payload.forEach((item) => { state.devicesItems[item.id] = item; });
  },
};

export default { state, getters, mutations };
