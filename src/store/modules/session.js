import useLocalStorage, { saveLocalStorage, deleteLocalStorage } from '@/common/utils/useLocalStorage';

// Elements
const state = {
  user: null,
  history: {},
  server: null,
  socket: null,
  positions: {},
  theme: useLocalStorage('theme')[0] ? useLocalStorage('theme')[0] : 'system',
  language: useLocalStorage('language')[0] ? useLocalStorage('language')[0] : 'en',
};

// Call elemnets on this module to another components
const getters = {
  user: (state) => state.user,
  theme: (state) => state.theme,
  socket: (state) => state.socket,
  server: (state) => state.server,
  history: (state) => state.history,
  language: (state) => state.language,
  positions: (state) => state.positions,
};

// Acctions
const mutations = {
  // Update user login data
  updateUser(state, payload) {
    state.user = payload;
  },
  // Update theme color page
  updateTheme(state, payload) {
    state.theme = payload;
    if (payload !== 'system') saveLocalStorage('theme', payload);
    else deleteLocalStorage('theme');
  },
  // Update languege selected
  updateLanguage(state, payload) {
    state.language = payload;
    saveLocalStorage('language', payload);
  },
  // Update server data
  updateServer(state, payload) {
    state.server = payload;
  },
  // Update socket data
  updateSocket(state, payload) {
    state.socket = payload;
  },
  // Update devices positions datas
  updatePositions(state, payload) {
    const liveRoutes = state.user ? state.user.attributes.mapLiveRoutes || state.server.attributes.mapLiveRoutes || 'none' : 'none';
    const liveRoutesLimit = 10;
    // state.user.attributes['web.liveRouteLength'] || state.user.attributes['web.liveRouteLength'] ||
    payload.forEach((position) => {
      state.positions[position.deviceId] = position;
      if (liveRoutes !== 'none') {
        const route = state.history[position.deviceId] || [];
        const last = route.at(-1);
        if (!last || (last[0] !== position.longitude && last[1] !== position.latitude)) {
          state.history[position.deviceId] = [...route.slice(1 - liveRoutesLimit), [position.longitude, position.latitude]];
        }
      } else {
        state.history = {};
      }
    });
  },
};

export default { state, getters, mutations };
