import { createStore } from 'vuex';
import devices from './modules/devices';
import events from './modules/events';
import session from './modules/session';

export default createStore({
  modules: { devices, events, session },
});
