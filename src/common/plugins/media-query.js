import {
  computed, onMounted, onUnmounted, ref,
} from 'vue';

export default function useBreakpoints() {
  // Datas uses
  const windowWidth = ref(window.innerWidth);
  const windowHeight = ref(window.innerHeight);
  const onWidthChange = () => {
    windowWidth.value = window.innerWidth;
  };
  const onHeightChange = () => {
    windowHeight.value = window.innerHeight;
  };
  // remove data if use
  onMounted(() => window.addEventListener('resize', onWidthChange));
  onMounted(() => window.addEventListener('resize', onHeightChange));
  // remove data if not use
  onUnmounted(() => window.removeEventListener('resize', onWidthChange));
  onUnmounted(() => window.removeEventListener('resize', onHeightChange));
  // type width window
  const type = computed(() => {
    if (windowWidth.value < 550) return 'xs';
    if (windowWidth.value >= 550 && windowWidth.value < 640) return 'sm';
    if (windowWidth.value >= 640 && windowWidth.value < 768) return 'md';
    if (windowWidth.value >= 768 && windowWidth.value < 1024) return 'lg';
    if (windowWidth.value >= 1024 && windowWidth.value < 1280) return 'xl';
    if (windowWidth.value >= 1536) return '2xl';
    return null; // This is an unreachable line, simply to keep eslint happy.
  });
  // detect if swap width
  const width = computed(() => windowWidth.value);
  const height = computed(() => windowHeight.value);
  // retrun values
  return { type, width, height };
}
