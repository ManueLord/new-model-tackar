import { ref } from 'vue';
import { useStore } from 'vuex';
import af from '@/assets/i18n/af.json';
import ak from '@/assets/i18n/ak.json';
import am from '@/assets/i18n/am.json';
import ar from '@/assets/i18n/ar.json';
import as from '@/assets/i18n/as.json';
import ay from '@/assets/i18n/ay.json';
import az from '@/assets/i18n/az.json';
import be from '@/assets/i18n/be.json';
import bg from '@/assets/i18n/bg.json';
import bho from '@/assets/i18n/bho.json';
import bm from '@/assets/i18n/bm.json';
import bn from '@/assets/i18n/bn.json';
import bs from '@/assets/i18n/bs.json';
import ca from '@/assets/i18n/ca.json';
import ceb from '@/assets/i18n/ceb.json';
import ckb from '@/assets/i18n/ckb.json';
import co from '@/assets/i18n/co.json';
import cs from '@/assets/i18n/cs.json';
import cy from '@/assets/i18n/cy.json';
import da from '@/assets/i18n/da.json';
import de from '@/assets/i18n/de.json';
import doi from '@/assets/i18n/doi.json';
import dv from '@/assets/i18n/dv.json';
import ee from '@/assets/i18n/ee.json';
import el from '@/assets/i18n/el.json';
import en from '@/assets/i18n/en.json';
import eo from '@/assets/i18n/eo.json';
import es from '@/assets/i18n/es.json';
import et from '@/assets/i18n/et.json';
import eu from '@/assets/i18n/eu.json';
import fa from '@/assets/i18n/fa.json';
import fi from '@/assets/i18n/fi.json';
import fr from '@/assets/i18n/fr.json';
import fy from '@/assets/i18n/fy.json';
import ga from '@/assets/i18n/ga.json';
import gd from '@/assets/i18n/gd.json';
import gl from '@/assets/i18n/gl.json';
import gn from '@/assets/i18n/gn.json';
import gom from '@/assets/i18n/gom.json';
import gu from '@/assets/i18n/gu.json';
import ha from '@/assets/i18n/ha.json';
import haw from '@/assets/i18n/haw.json';
import hi from '@/assets/i18n/hi.json';
import hmn from '@/assets/i18n/hmn.json';
import hr from '@/assets/i18n/hr.json';
import ht from '@/assets/i18n/ht.json';
import hu from '@/assets/i18n/hu.json';
import hy from '@/assets/i18n/hy.json';
import id from '@/assets/i18n/id.json';
import ig from '@/assets/i18n/ig.json';
import ilo from '@/assets/i18n/ilo.json';
import is from '@/assets/i18n/is.json';
import it from '@/assets/i18n/it.json';
import iw from '@/assets/i18n/iw.json';
import ja from '@/assets/i18n/ja.json';
import jw from '@/assets/i18n/jw.json';
import ka from '@/assets/i18n/ka.json';
import kk from '@/assets/i18n/kk.json';
import km from '@/assets/i18n/km.json';
import kn from '@/assets/i18n/kn.json';
import ko from '@/assets/i18n/ko.json';
import kri from '@/assets/i18n/kri.json';
import ku from '@/assets/i18n/ku.json';
import ky from '@/assets/i18n/ky.json';
import la from '@/assets/i18n/la.json';
import lb from '@/assets/i18n/lb.json';
import lg from '@/assets/i18n/lg.json';
import ln from '@/assets/i18n/ln.json';
import lo from '@/assets/i18n/lo.json';
import lt from '@/assets/i18n/lt.json';
import lus from '@/assets/i18n/lus.json';
import lv from '@/assets/i18n/lv.json';
import mai from '@/assets/i18n/mai.json';
import mg from '@/assets/i18n/mg.json';
import mi from '@/assets/i18n/mi.json';
import mk from '@/assets/i18n/mk.json';
import ml from '@/assets/i18n/ml.json';
import mn from '@/assets/i18n/mn.json';
import mniMtei from '@/assets/i18n/mni-Mtei.json';
import mr from '@/assets/i18n/mr.json';
import ms from '@/assets/i18n/ms.json';
import mt from '@/assets/i18n/mt.json';
import my from '@/assets/i18n/my.json';
import ne from '@/assets/i18n/ne.json';
import nl from '@/assets/i18n/nl.json';
import no from '@/assets/i18n/no.json';
import nso from '@/assets/i18n/nso.json';
import ny from '@/assets/i18n/ny.json';
import om from '@/assets/i18n/om.json';
import or from '@/assets/i18n/or.json';
import pa from '@/assets/i18n/pa.json';
import pl from '@/assets/i18n/pl.json';
import ps from '@/assets/i18n/ps.json';
import pt from '@/assets/i18n/pt.json';
import qu from '@/assets/i18n/qu.json';
import ro from '@/assets/i18n/ro.json';
import ru from '@/assets/i18n/ru.json';
import rw from '@/assets/i18n/rw.json';
import sa from '@/assets/i18n/sa.json';
import sd from '@/assets/i18n/sd.json';
import si from '@/assets/i18n/si.json';
import sk from '@/assets/i18n/sk.json';
import sl from '@/assets/i18n/sl.json';
import sm from '@/assets/i18n/sm.json';
import sn from '@/assets/i18n/sn.json';
import so from '@/assets/i18n/so.json';
import sq from '@/assets/i18n/sq.json';
import sr from '@/assets/i18n/sr.json';
import st from '@/assets/i18n/st.json';
import su from '@/assets/i18n/su.json';
import sv from '@/assets/i18n/sv.json';
import sw from '@/assets/i18n/sw.json';
import ta from '@/assets/i18n/ta.json';
import te from '@/assets/i18n/te.json';
import tg from '@/assets/i18n/tg.json';
import th from '@/assets/i18n/th.json';
import ti from '@/assets/i18n/ti.json';
import tk from '@/assets/i18n/tk.json';
import tl from '@/assets/i18n/tl.json';
import tr from '@/assets/i18n/tr.json';
import ts from '@/assets/i18n/ts.json';
import tt from '@/assets/i18n/tt.json';
import ug from '@/assets/i18n/ug.json';
import uk from '@/assets/i18n/uk.json';
import ur from '@/assets/i18n/ur.json';
import uz from '@/assets/i18n/uz.json';
import vi from '@/assets/i18n/vi.json';
import xh from '@/assets/i18n/xh.json';
import yi from '@/assets/i18n/yi.json';
import yo from '@/assets/i18n/yo.json';
import zhCN from '@/assets/i18n/zh-CN.json';
import zhTW from '@/assets/i18n/zh-TW.json';
import zu from '@/assets/i18n/zu.json';
// Languages
const langs = {
  af: { data: af, name: 'Afrikaans' },
  ak: { data: ak, name: 'Twi' },
  am: { data: am, name: '\u12a0\u121b\u122d\u129b' },
  ar: { data: ar, name: '\u0639\u0631\u0628\u0649' },
  as: { data: as, name: '\u0985\u09b8\u09ae\u09c0\u09df\u09be' },
  ay: { data: ay, name: 'Aymara' },
  az: { data: az, name: 'Az\u0259rbaycanl\u0131' },
  be: { data: be, name: '\u0431\u0435\u043b\u0430\u0440\u0443\u0441\u043a\u0456' },
  bg: { data: bg, name: '\u0431\u044a\u043b\u0433\u0430\u0440\u0441\u043a\u0438' },
  bho: { data: bho, name: '\u092d\u094b\u091c\u092a\u0941\u0930\u0940' },
  bm: { data: bm, name: 'Ka don' },
  bn: { data: bn, name: '\u09ac\u09be\u0982\u09b2\u09be' },
  bs: { data: bs, name: 'Bosanski' },
  ca: { data: ca, name: 'Catal\u00e0' },
  ceb: { data: ceb, name: 'Cebuano' },
  ckb: { data: ckb, name: '\u06a9\u0648\u0631\u062f\u06cc (\u0633\u06c6\u0631\u0627\u0646\u06cc)' },
  co: { data: co, name: 'Corsu' },
  cs: { data: cs, name: '\u010de\u0161tina' },
  cy: { data: cy, name: 'Cymraeg' },
  da: { data: da, name: 'Dansk' },
  de: { data: de, name: 'Deutsche' },
  doi: { data: doi, name: '\u0921\u094b\u0917\u0930\u0940' },
  dv: { data: dv, name: '\u078b\u07a8\u0788\u07ac\u0780\u07a8' },
  ee: { data: ee, name: 'E\u028begbe' },
  el: { data: el, name: '\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac' },
  en: { data: en, name: 'English' },
  eo: { data: eo, name: 'Esperanto' },
  es: { data: es, name: 'Espa\u00f1ol' },
  et: { data: et, name: 'Eesti keel' },
  eu: { data: eu, name: 'Euskalduna' },
  fa: { data: fa, name: '\u0641\u0627\u0631\u0633\u06cc' },
  fi: { data: fi, name: 'Suomalainen' },
  fr: { data: fr, name: 'Fran\u00e7ais' },
  fy: { data: fy, name: 'Frysk' },
  ga: { data: ga, name: 'Gaeilge' },
  gd: { data: gd, name: 'G\u00e0idhlig na h-Alba' },
  gl: { data: gl, name: 'Galego' },
  gn: { data: gn, name: 'Guarani' },
  gom: { data: gom, name: '\u0915\u094b\u0902\u0915\u0923\u0940' },
  gu: { data: gu, name: '\u0a97\u0ac1\u0a9c\u0ab0\u0abe\u0aa4\u0ac0' },
  ha: { data: ha, name: 'Hausa' },
  haw: { data: haw, name: '\u02bb\u014clelo Hawai\u02bbi' },
  hi: { data: hi, name: '\u0939\u093f\u0902\u0926\u0940' },
  hmn: { data: hmn, name: 'Hmoob' },
  hr: { data: hr, name: 'Hrvatski' },
  ht: { data: ht, name: 'Kreyol ayisyen' },
  hu: { data: hu, name: 'Magyar' },
  hy: { data: hy, name: '\u0570\u0561\u0575\u0565\u0580\u0565\u0576' },
  id: { data: id, name: 'Bahasa Indonesia' },
  ig: { data: ig, name: 'Igbo' },
  ilo: { data: ilo, name: 'Ilocano' },
  is: { data: is, name: '\u00edslensku' },
  it: { data: it, name: 'Italiano' },
  iw: { data: iw, name: '\u05e2\u05b4\u05d1\u05e8\u05b4\u05d9\u05ea' },
  ja: { data: ja, name: '\u65e5\u672c' },
  jw: { data: jw, name: 'Basa jawa' },
  ka: { data: ka, name: '\u10e5\u10d0\u10e0\u10d7\u10d5\u10d4\u10da\u10d8' },
  kk: { data: kk, name: '\u049b\u0430\u0437\u0430\u049b' },
  km: { data: km, name: '\u1781\u17d2\u1798\u17c2\u179a' },
  kn: { data: kn, name: '\u0c95\u0ca8\u0ccd\u0ca8\u0ca1' },
  ko: { data: ko, name: '\ud55c\uad6d\uc5b4' },
  kri: { data: kri, name: 'Ayd' },
  ku: { data: ku, name: 'Kurd\u00ee' },
  ky: { data: ky, name: '\u041a\u044b\u0440\u0433\u044b\u0437\u0447\u0430' },
  la: { data: la, name: 'Latin' },
  lb: { data: lb, name: 'L\u00ebtzebuergesch' },
  lg: { data: lg, name: 'Luganda' },
  ln: { data: ln, name: 'Lingala' },
  lo: { data: lo, name: '\u0ea5\u0eb2\u0ea7' },
  lt: { data: lt, name: 'Lietuvis' },
  lus: { data: lus, name: 'Mizo' },
  lv: { data: lv, name: 'Latviski' },
  mai: { data: mai, name: '\u092e\u0948\u0925\u093f\u0932\u0940' },
  mg: { data: mg, name: 'Malagasy' },
  mi: { data: mi, name: 'Maori' },
  mk: { data: mk, name: '\u043c\u0430\u043a\u0435\u0434\u043e\u043d\u0441\u043a\u0438' },
  ml: { data: ml, name: '\u0d2e\u0d32\u0d2f\u0d3e\u0d33\u0d02' },
  mn: { data: mn, name: '\u043c\u043e\u043d\u0433\u043e\u043b' },
  mniMtei: { data: mniMtei, name: '\uabc3\uabe6\uabcf\uabc7\uabe6\uabcf\uabc2\uabe3\uabdf (\uabc3\uabc5\uabe4\uabc4\uabe8\uabd4\uabe4)' },
  mr: { data: mr, name: '\u092e\u0930\u093e\u0920\u0940' },
  ms: { data: ms, name: 'Melayu' },
  mt: { data: mt, name: 'Malti' },
  my: { data: my, name: '\u1019\u103c\u1014\u103a\u1019\u102c' },
  ne: { data: ne, name: '\u0928\u0947\u092a\u093e\u0932\u0940' },
  nl: { data: nl, name: 'Nederlands' },
  no: { data: no, name: 'Norsk' },
  nso: { data: nso, name: 'sepedi' },
  ny: { data: ny, name: 'Chichewa' },
  om: { data: om, name: 'Afaan Oromoo' },
  or: { data: or, name: '\u0b13\u0b21\u0b3f\u0b06 (\u0b13\u0b21\u0b3f\u0b06)' },
  pa: { data: pa, name: '\u0a2a\u0a70\u0a1c\u0a3e\u0a2c\u0a40' },
  pl: { data: pl, name: 'Polskie' },
  ps: { data: ps, name: '\u067e\u069a\u062a\u0648' },
  pt: { data: pt, name: 'Portugu\u00eas' },
  qu: { data: qu, name: 'Runasimi' },
  ro: { data: ro, name: 'Rom\u00e2n\u0103' },
  ru: { data: ru, name: '\u0440\u0443\u0441\u0441\u043a\u0438\u0439' },
  rw: { data: rw, name: 'Kinyarwanda' },
  sa: { data: sa, name: '\u0938\u0902\u0938\u094d\u0915\u0943\u0924' },
  sd: { data: sd, name: '\u0633\u0646\u068c\u064a' },
  si: { data: si, name: '\u0dc3\u0dd2\u0d82\u0dc4\u0dbd' },
  sk: { data: sk, name: 'Sloven\u010dina' },
  sl: { data: sl, name: 'Sloven\u0161\u010dina' },
  sm: { data: sm, name: 'Samoa' },
  sn: { data: sn, name: 'Shona' },
  so: { data: so, name: 'Somaliyeed' },
  sq: { data: sq, name: 'Shqip' },
  sr: { data: sr, name: '\u0421\u0440\u043f\u0441\u043a\u0438' },
  st: { data: st, name: 'Sesotho' },
  su: { data: su, name: 'Sundanese' },
  sv: { data: sv, name: 'Svenska' },
  sw: { data: sw, name: 'Kiswahili' },
  ta: { data: ta, name: '\u0ba4\u0bae\u0bbf\u0bb4\u0bcd' },
  te: { data: te, name: '\u0c24\u0c46\u0c32\u0c41\u0c17\u0c41' },
  tg: { data: tg, name: '\u0442\u043e\u04b7\u0438\u043a\u04e3' },
  th: { data: th, name: '\u0e41\u0e1a\u0e1a\u0e44\u0e17\u0e22' },
  ti: { data: ti, name: '\u1275\u130d\u122a\u129b' },
  tk: { data: tk, name: 'T\u00fcrkmen' },
  tl: { data: tl, name: 'Pilipino' },
  tr: { data: tr, name: 'T\u00fcrk\u00e7e' },
  ts: { data: ts, name: 'Tsonga' },
  tt: { data: tt, name: '\u0442\u0430\u0442\u0430\u0440' },
  ug: { data: ug, name: 'Uyghur' },
  uk: { data: uk, name: '\u0443\u043a\u0440\u0430\u0457\u043d\u0441\u044c\u043a\u0430' },
  ur: { data: ur, name: '\u0627\u0631\u062f\u0648' },
  uz: { data: uz, name: 'O`zbek' },
  vi: { data: vi, name: 'Ti\u1ebfng Vi\u1ec7t' },
  xh: { data: xh, name: 'IsiXhosa' },
  yi: { data: yi, name: '\u05d9\u05d9\u05b4\u05d3\u05d9\u05e9' },
  yo: { data: yo, name: 'Yoruba' },
  zhCN: { data: zhCN, name: '\u7b80\u4f53\u4e2d\u6587\uff09' },
  zhTW: { data: zhTW, name: '\u4e2d\u570b\u50b3\u7d71\u7684\uff09' },
  zu: { data: zu, name: 'Zulu' },
};

function translation() {
  // Datas uses
  const languages = langs;
  const store = useStore();
  // Funcition if change language
  const setLanguage = (lang) => {
    store.commit('updateLanguage', lang);
    // saveLocalStorage('language', lang);
  };
  // Function tranlete word indicate for language that use
  /* jshint sub:true */
  const t = (word) => {
    const lan = ref(store.getters.language);
    if (lan.value === 'null') {
      lan.value = 'en';
      setLanguage('en');
    }
    return languages[lan.value].data[word];
  };
  // return values
  return { t, setLanguage, languages };
}

export default translation;
