import { ref, watchEffect } from 'vue';

// Previous option
export const previous = (value) => {
  const refrence = ref();
  watchEffect(() => {
    refrence.value = value;
  });
  return refrence.value;
};
// Call back
export const catchCallback = () => null;
