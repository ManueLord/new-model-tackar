import { useRouter } from 'vue-router';
import translation from '@/common/plugins/languages';
import { useQuasar } from 'quasar';
import { ref, watch, watchEffect } from 'vue';
import { useStore } from 'vuex';
import usFeatures from '@/common/utils/feactures';
import alarm from '@/assets/audios/alarm.mp3';
import { attributePreference } from '../utils/preferences';

const SocketController = () => {
  // Datas uses
  const $q = useQuasar();
  const events = ref([]);
  const socketRef = ref();
  const logoutCode = 4000;
  const store = useStore();
  const router = useRouter();
  const { t } = translation();
  const features = usFeatures();
  const notifications = ref([]);
  const authenticated = ref(store.getters.user);
  const soundEvents = attributePreference('soundEvents', '');
  const soundAlarms = attributePreference('soundAlarms', 'sos');
  // Function for connect on WebSocket
  const connectSocket = () => {
    // Change portocol page
    const protocol = window.location.protocol === 'https:' ? 'wss:' : 'ws:';
    // Connect socket
    const socket = new WebSocket(`${protocol}//${window.location.host}/api/socket`);
    socketRef.value = socket;
    // Socket open
    socket.onopen = () => {
      store.commit('updateSocket', true);
    };
    // Socket close
    socket.onclose = async (event) => {
      store.commit('updateSocket', false);
      if (event.code !== logoutCode) {
        try {
          const devicesResponse = await fetch('/api/devices');
          if (devicesResponse.ok) store.commit('devicesUpdate', await devicesResponse.json());
          const positionsResponse = await fetch('/api/positions');
          if (positionsResponse.ok) store.commit('updatePositions', await positionsResponse.json());
          if (devicesResponse.status === 401 || positionsResponse.status === 401) router.push({ name: 'login' });
        } catch (error) {
          // ignore errors
        }
        setTimeout(() => connectSocket(), 60000);
      }
    };
    // Socket deletect message
    socket.onmessage = (event) => {
      const data = JSON.parse(event.data);
      // message device
      if (data.devices) {
        store.commit('devicesUpdate', data.devices);
      }
      // message position
      if (data.positions) {
        store.commit('updatePositions', data.positions);
      }
      // message event
      if (data.events) {
        if (!features.disableEvents) store.commit('eventsAdd', data.events);
        events.value = data.events;
      }
    };
  };
  // Function validad if user is auth
  watchEffect(async () => {
    if (authenticated.value) {
      const response = await fetch('/api/devices');
      if (response.ok) {
        store.commit('devicesRefresh', await response.json());
      } else {
        throw Error(await response.text());
      }
      connectSocket();
    } else {
      const socket = socketRef.value;
      if (socket) {
        socket.close(logoutCode);
      }
    }
  });
  // Function detect if have event socket
  watchEffect(() => {
    notifications.value = events.value.map((event) => ({
      id: event.id,
      message: event.attributes.message,
      show: true,
    }));
  }, [t]);
  // Fuction detect if have event and play sound
  watchEffect(() => {
    events.value.forEach((event) => {
      if (soundEvents.includes(event.type) || (event.type === 'alarm' && soundAlarms.includes(event.attributes.alarm))) {
        new Audio(alarm).play();
      }
    });
  });
  // Fuction detect if have event and show massage on view
  watchEffect(() => {
    notifications.value.map((notification) => $q.notify({ message: notification.message, classes: 'max-w-[400px]' }));
  });
  // Valid if change data user
  watch(() => store.getters.user, (u) => {
    authenticated.value = u;
  });
  const style = document.createElement('style');
  style.innerHTML = `
    .q-notifications__list--bottom {
      bottom: 0;
      margin-bottom: 4rem;
    };
  `;
  document.head.appendChild(style);
};

export default SocketController;
