import { useStore } from 'vuex';

export const useAdministrator = () => {
  const store = useStore();
  const admin = store.getters.user.administrator;
  return admin;
};

export const useManager = () => {
  const store = useStore();
  const admin = store.getters.user.administrator;
  const manager = (store.getters.user.userLimit || 0) !== 0;
  return admin || manager;
};

export const useDeviceReadonly = () => {
  const store = useStore();
  const admin = store.getters.user.administrator;
  const serverReadonly = store.getters.server.readonly;
  const userReadonly = store.getters.user.readonly;
  const serverDeviceReadonly = store.getters.server.deviceReadonly;
  const userDeviceReadonly = store.getters.user.deviceReadonly;
  return !admin && (serverReadonly || userReadonly || serverDeviceReadonly || userDeviceReadonly);
};

export const useRestriction = (key) => {
  const store = useStore();
  const admin = store.getters.user.administrator;
  const serverValue = store.getters.server[key];
  const userValue = store.getters.user[key];
  return !admin && (serverValue || userValue);
};
