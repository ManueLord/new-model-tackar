import { useStore } from 'vuex';

const get = (server, user, key) => {
  if (server && user) {
    if (user.administrator) return false;
    if (server.forceSettings) return server.attributes[key] || user.attributes[key] || false;
    return user.attributes[key] || server.attributes[key] || false;
  }
  return false;
};

export default () => {
  // Datas uses
  const store = useStore();
  const user = store.getters.server;
  const serve = store.getters.server;
  // Disable elements
  const disableAttributes = get(serve, user, 'ui.disableAttributes');
  const disableVehicleFetures = get(serve, user, 'ui.disableVehicleFetures');
  const disableDrivers = disableVehicleFetures || get(serve, user, 'ui.disableDrivers');
  const disableMaintenance = disableVehicleFetures || get(serve, user, 'ui.disableMaintenance');
  const disableGroups = get(serve, user, 'ui.disableGroups');
  const disableEvents = get(serve, user, 'ui.disableEvents');
  const disableComputedAttributes = get(serve, user, 'ui.disableComputedAttributes');
  const disableCalendars = get(serve, user, 'ui.disableCalendars');
  // Retrun values
  return {
    disableAttributes,
    disableDrivers,
    disableMaintenance,
    disableGroups,
    disableEvents,
    disableComputedAttributes,
    disableCalendars,
  };
};
