import { watchEffect } from 'vue';
// Update value on local storage
export const saveLocalStorage = (key, value) => {
  window.localStorage.setItem(key, JSON.stringify(value));
};
// Delete value on local storage
export const deleteLocalStorage = (key) => window.localStorage.removeItem(key);
// Read value or create value on local storage
export default (key, defaultValue) => {
  const value = () => {
    const stickyValue = window.localStorage.getItem(key);
    return stickyValue ? JSON.parse(stickyValue) : defaultValue;
  };
  watchEffect(() => {
    if (value() !== defaultValue) {
      saveLocalStorage(key, value());
    } else {
      window.localStorage.removeItem(key);
    }
  });

  return [value()];
};
