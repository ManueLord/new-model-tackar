import moment from 'moment';

// Format time for 12 horus
export const formatTime = (value, format, hours12) => {
  if (value) {
    const m = moment(value);
    switch (format) {
      case 'date':
        return m.format('YYYY-MM-DD');
      case 'time':
        return m.format(hours12 ? 'hh:mm:ss A' : 'HH:mm:ss');
      case 'minutes':
        return m.format(hours12 ? 'YYYY-MM-DD hh:mm A' : 'YYYY-MM-DD HH:mm');
      default:
        return m.format(hours12 ? 'YYYY-MM-DD hh:mm:ss A' : 'YYYY-MM-DD HH:mm:ss');
    }
  }
  return '';
};
// Status color on markers on map
export const getStatusColorMap = (status) => {
  switch (status) {
    case 'online':
      return '#008102';
    case 'offline':
      return '#BC0000';
    case 'unknown':
      return '#FF8300';
    default:
      return '#8C8C8C';
  }
};
// Status color on markers on route
export const getStatusColorRoute = (status) => {
  switch (status) {
    case 'route':
      return '#0036FF';
    case 'end':
      return '#BC0000';
    case 'start':
      return '#008102';
    case 'special':
      return '#9500BA';
    default:
      return '#8C8C8C';
  }
};
