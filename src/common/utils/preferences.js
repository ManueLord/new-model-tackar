import { useStore } from 'vuex';

// eslint-disable-next-line
const containsProperty = (object, key) => object.hasOwnProperty(key) && object[key] !== null;
// Use datos con preference of the user login
export const usePreference = (key, defaultValue) => {
  const store = useStore();
  if (store.getters.server.forceSettings) {
    if (containsProperty(store.getters.server, key)) return store.getters.server[key];
    if (containsProperty(store.getters.user, key)) return store.getters.user[key];
    return defaultValue;
  }
  if (containsProperty(store.getters.server, key)) return store.getters.server[key];
  if (containsProperty(store.getters.user, key)) return store.getters.user[key];
  return defaultValue;
};
// Use datos con attributes of the user login
export const attributePreference = (key, defaultValue) => {
  const store = useStore();
  if (store.getters.server.forceSettings) {
    if (containsProperty(store.getters.server.attributes, key)) return store.getters.server.attributes[key];
    if (containsProperty(store.getters.user.attributes, key)) return store.getters.user.attributes[key];
    return defaultValue;
  }
  if (containsProperty(store.getters.server.attributes, key)) return store.getters.server.attributes[key];
  if (containsProperty(store.getters.user.attributes, key)) return store.getters.user.attributes[key];
  return defaultValue;
};
