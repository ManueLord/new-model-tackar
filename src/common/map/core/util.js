// Load image
export const loadImg = (url) => new Promise((imgLoaded) => {
  const image = new Image();
  image.onload = () => imgLoaded(image);
  image.src = url;
});
// Change color of image
const canvasTintImage = (image, color) => {
  const canvas = document.createElement('canvas');
  canvas.width = image.width * devicePixelRatio;
  canvas.height = image.height * devicePixelRatio;
  canvas.style.width = `${image.width}px`;
  canvas.style.height = `${image.height}px`;
  const context = canvas.getContext('2d');
  context.save();
  context.fillStyle = color;
  context.globalAlpha = 1;
  context.fillRect(0, 0, canvas.width, canvas.height);
  context.globalCompositeOperation = 'destination-atop';
  context.globalAlpha = 1;
  context.drawImage(image, 0, 0, canvas.width, canvas.height);
  context.restore();

  return canvas;
};
// Create image with background
export const makeIconWithBackground = (background, icon, color) => {
  const canvas = document.createElement('canvas');
  canvas.width = background.width * devicePixelRatio;
  canvas.height = background.height * devicePixelRatio;
  canvas.style.width = `${background.width}px`;
  canvas.style.height = `${background.height}px`;
  const context = canvas.getContext('2d');
  context.drawImage(background, 0, 0, canvas.width, canvas.height);
  if (icon) {
    const iconRatio = 0.5;
    const imgWidth = canvas.width * iconRatio;
    const imgHeight = canvas.height * iconRatio;
    context.drawImage(canvasTintImage(icon, color), (canvas.width - imgWidth) / 2, (canvas.height - imgHeight) / 2, imgWidth, imgHeight);
  }
  return context.getImageData(0, 0, canvas.width, canvas.height);
};
// Create image without background
export const makeIconWithoutBackground = (icon, color) => {
  const canvas = document.createElement('canvas');
  canvas.width = icon.width * devicePixelRatio;
  canvas.height = icon.height * devicePixelRatio;
  canvas.style.width = `${icon.width}px`;
  canvas.style.height = `${icon.height}px`;
  const context = canvas.getContext('2d');
  context.save();
  context.shadowColor = 'black';
  context.shadowBlur = '15';
  context.fillStyle = color;
  context.globalAlpha = 1;
  context.fillRect(0, 0, canvas.width, canvas.height);
  context.globalCompositeOperation = 'destination-atop';
  context.globalAlpha = 1;
  context.drawImage(icon, 0, 0, canvas.width, canvas.height);
  context.restore();
  return context.getImageData(0, 0, canvas.width, canvas.height);
};
