import { getStatusColorMap, getStatusColorRoute } from '@/common/utils/formatter';
import backgroundSvg from '@/assets/images/icons/background.svg';
import directionSvg from '@/assets/images/icons/direction.svg';
import animalSvg from '@/assets/images/icons/marker/animal.svg';
import arrowSvg from '@/assets/images/icons/marker/arrow.svg';
import bicycleSvg from '@/assets/images/icons/marker/bicycle.svg';
import boatSvg from '@/assets/images/icons/marker/boat.svg';
import busSvg from '@/assets/images/icons/marker/bus.svg';
import carSvg from '@/assets/images/icons/marker/car.svg';
import craneSvg from '@/assets/images/icons/marker/crane2.svg';
import defaultSvg from '@/assets/images/icons/marker/default.svg';
import helicopterSvg from '@/assets/images/icons/marker/helicopter.svg';
import markerSvg from '@/assets/images/icons/marker/marker.svg';
import motorcycleSvg from '@/assets/images/icons/marker/motorcycle.svg';
import offroadSvg from '@/assets/images/icons/marker/offroad.svg';
import personSvg from '@/assets/images/icons/marker/person.svg';
import pickupSvg from '@/assets/images/icons/marker/pickup.svg';
import planeSvg from '@/assets/images/icons/marker/plane.svg';
import scooterSvg from '@/assets/images/icons/marker/scooter.svg';
import shipSvg from '@/assets/images/icons/marker/ship.svg';
import tractorSvg from '@/assets/images/icons/marker/tractor.svg';
import trainSvg from '@/assets/images/icons/marker/train.svg';
import tramSvg from '@/assets/images/icons/marker/tram.svg';
import trolleybusSvg from '@/assets/images/icons/marker/trolleybus.svg';
import truckSvg from '@/assets/images/icons/marker/truck.svg';
import vanSvg from '@/assets/images/icons/marker/van.svg';
import { loadImg, makeIconWithBackground, makeIconWithoutBackground } from './util';

// Icons with background
export const mapIconsBackground = {
  animal: animalSvg,
  bicycle: bicycleSvg,
  boat: boatSvg,
  bus: busSvg,
  car: carSvg,
  crane: craneSvg,
  default: defaultSvg,
  helicopter: helicopterSvg,
  motorcycle: motorcycleSvg,
  offroad: offroadSvg,
  person: personSvg,
  pickup: pickupSvg,
  plane: planeSvg,
  scooter: scooterSvg,
  ship: shipSvg,
  tractor: tractorSvg,
  train: trainSvg,
  tram: tramSvg,
  trolleybus: trolleybusSvg,
  truck: truckSvg,
  van: vanSvg,
};
// Icons without background
export const mapIconsSimple = {
  arrow: arrowSvg,
  marker: markerSvg,
};
// Use this export if you charge icon that not charge swap for default icons
// eslint-disable-next-line
export const mapIconsKey = (category) => (mapIconsBackground.hasOwnProperty(category) ? category : 'default');
// eslint-disable-next-line
export const mapIconsKeyRoute = (category) => (mapIconsSimple.hasOwnProperty(category) ? category : 'marker');
// Export element withc all icons
export const mapImgs = {};
// Element principal
export default async () => {
  // Load images backgroud
  const background = await loadImg(backgroundSvg);
  mapImgs.background = await makeIconWithBackground(background);
  const direction = await loadImg(directionSvg);
  mapImgs.direction = await makeIconWithBackground(direction);
  // Change icons on map without background
  await Promise.all(Object.keys(mapIconsSimple).map(async (category) => {
    const effects = [];
    ['route', 'end', 'start', 'special'].forEach((color) => {
      effects.push(loadImg(mapIconsSimple[category]).then((icon) => {
        mapImgs[`${category}-${color}`] = makeIconWithoutBackground(icon, getStatusColorRoute(color));
      }));
    });
    await Promise.all(effects);
  }));
  // Charge icons on map with background
  await Promise.all(Object.keys(mapIconsBackground).map(async (category) => {
    const effects = [];
    ['online', 'offline', 'unknown', 'neutral'].forEach((color) => {
      effects.push(loadImg(mapIconsBackground[category]).then((icon) => {
        mapImgs[`${category}-${color}`] = makeIconWithBackground(background, icon, getStatusColorMap(color));
      }));
    });
    await Promise.all(effects);
  }));
};
