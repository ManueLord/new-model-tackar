import { previous } from '@/common/server/helper';
import { attributePreference } from '@/common/utils/preferences';
import { watch, watchEffect } from 'vue';
import { useStore } from 'vuex';
import { map } from '@/components/map/MapMain.vue';

const SelectedDevice = () => {
  const store = useStore();
  const selectedDeviceId = store.getters.deviceSelectedId;
  const previousDeviceId = previous(selectedDeviceId);

  const selectZoom = attributePreference('web.selectZoom', 15);
  const mapFollow = attributePreference('mapFollow', false);

  const position = store.getters.positions[selectedDeviceId];

  watch(() => store.getters.deviceSelectedId, (id) => {
    console.log(store.getters);
    console.log('------------------------');
    console.log(id);
    console.log('------------------------');
  });

  watchEffect(() => {
    console.log(selectedDeviceId);
    if ((selectedDeviceId !== previousDeviceId || mapFollow) && position) {
      map.easeTo({
        center: [position.longitude, position.latitude],
        zoom: Math.max(map.getZoom(), selectZoom),
        offset: [0, -300 / 2],
        // offset: [0, -dimensions.popupMapOffset / 2],300
      });
    }
  });

  return null;
};

export default SelectedDevice;
