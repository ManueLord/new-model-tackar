/* eslint-disable */
export default class MapLibreStyleSwitcherControl {
  constructor(beforeSwitch, onSwitch, afterSwitch) {
    this.beforeSwitch = beforeSwitch;
    this.onSwitch = onSwitch;
    this.afterSwitch = afterSwitch;
    this.documentClick = this.documentClick.bind(this);
    this.styles = [];
    this.actualStyle = null;
  }
  // position
  getDefaultPosition() {
    return 'top-right';
  }
  // funcion if switch style map
  updatedStyles(newStyle, oldStyle) {
    this.styles = newStyle;
    let selectedStyle = null;
    for (const style of this.styles) {
      if (style.id === (this.actualStyle || oldStyle)) {
        selectedStyle = style.id;
        break;
      }
    }
    if (!selectedStyle) selectedStyle = this.styles[0].id;
    while (this.mapStyleContainer.firstChild) {
      this.mapStyleContainer.removeChild(this.mapStyleContainer.firstChild);
    }
    let selectedStyleElement;
    for (const style of this.styles) {
      const styleElement = document.createElement('button');
      styleElement.innerText = style.title;
      styleElement.dataset.id = style.id;
      styleElement.dataset.style = JSON.stringify(style.url);
      styleElement.addEventListener('click', (event) => {
        const { target } = event;
        if (!target.classList.contains('active')) this.onSelectedStyle(target);
      });
      if (style.id === selectedStyle) {
        selectedStyleElement = styleElement;
        styleElement.classList.add('active');
      }
      this.mapStyleContainer.appendChild(styleElement);
    }
    if (this.actualStyle !== selectedStyle) {
      this.onSelectedStyle(selectedStyleElement);
      this.actualStyle = selectedStyle;
    }
  }
  // function for this style use
  onSelectedStyle(target) {
    this.beforeSwitch();
    const style = this.styles.find((it) => it.id === target.dataset.id);
    this.map.setStyle(style.url, {diff: false});
    this.map.setTransformRequest(style.transformRequest);
    this.onSwitch(target.dataset.id);
    this.mapStyleContainer.style.display = 'none';
    this.styleButton.style.display = 'block';
    const elements = this.mapStyleContainer.getElementsByClassName('active');
    while(elements[0]){
      elements[0].classList.remove('active');
    }
    target.classList.add('active');
    this.actualStyle = target.dataset.id;
    this.afterSwitch();
  }
  // Create element for view on the page
  onAdd(map) {
    this.map = map;
    this.controlContainer = document.createElement('div');
    this.controlContainer.classList.add('maplibregl-ctrl');
    this.controlContainer.classList.add('maplibregl-ctrl-group');
    this.mapStyleContainer = document.createElement('div');
    this.controlContainer.style.color = 'black';
    this.styleButton = document.createElement('button');
    this.styleButton.type = 'button';
    this.mapStyleContainer.classList.add('maplibregl-style-list');
    this.styleButton.classList.add('maplibregl-ctrl-icon');
    this.styleButton.classList.add('maplibregl-style-switcher');
    this.styleButton.addEventListener('click', () => {
      this.styleButton.style.display = 'none';
      this.mapStyleContainer.style.display = 'block';
    });
    document.addEventListener('click', this.documentClick);
    this.controlContainer.appendChild(this.styleButton);
    this.controlContainer.appendChild(this.mapStyleContainer);
    return this.controlContainer;
  }
  // remove element before if change style
  onRemove() {
    if (!this.controlContainer || !this.controlContainer.parentNode || !this.map || !this.styleButton) {
      return;
    }
    this.styleButton.removeEventListener('click', this.onDocumentClick);
    this.controlContainer.parentNode.removeChild(this.controlContainer);
    document.removeEventListener('click', this.documentClick);
    this.map = undefined;
  }
  // Action of this elemets if press new style
  documentClick(event) {
    if (this.controlContainer && !this.controlContainer.contains(event.target) && this.mapStyleContainer && this.styleButton) {
      this.mapStyleContainer.style.display = 'none';
      this.styleButton.style.display = 'block';
    }
  }
}
