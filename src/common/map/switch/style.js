export default function mapStyle() {
  // const key = ''
  // style costume map
  const styleCustom = (urls) => ({
    version: 8,
    sources: {
      custom: {
        type: 'raster',
        tiles: urls,
        tileSize: 256,
      },
    },
    glyphs: 'https://cdn.traccar.com/map/fonts/{fontstack}/{range}.pbf',
    layers: [{
      id: 'custom',
      type: 'raster',
      source: 'custom',
    }],
  });
  // templete styles map
  const titleStyles = [
    {
      id: 'osm',
      title: 'Open Street Map',
      url: styleCustom(['https://a.tile.openstreetmap.org/{z}/{x}/{y}.png']),
    },
    // {
    //     title: "Baisc",
    //     uri:
    //         "https://api.maptiler.com/maps/basic/style.json?key=" +
    //         key,
    // },
    // {
    //     title: "Light",
    //     uri:
    //         "https://api.maptiler.com/maps/bright/style.json?key=" +
    //         key,
    // },
    // {
    //     title: "Outdoors",
    //     uri:
    //         "https://api.maptiler.com/maps/outdoor/style.json?key=" +
    //         key,
    // },
    {
      id: 'googleRoad',
      title: 'Google Road',
      url: styleCustom(['https://mt0.google.com/vt/lyrs=m,traffic&hl=en&x={x}&y={y}&z={z}&s=Galil']),
    },
    {
      id: 'googleSatellite',
      title: 'Google Satellite',
      url: styleCustom(['http://mt0.google.com/vt/lyrs=y,traffic&hl=en&x={x}&y={y}&z={z}&s=Galil']),
    },
    // {
    //     title: 'mapMapTilerHybrid',
    //     uri: `https://api.maptiler.com/maps/hybrid/style.json?key=${key}`,
    // },
    // {
    //     title: "Demotiles",
    //     uri: "https://demotiles.maplibre.org/style.json",
    // },
  ];

  return titleStyles;
}
