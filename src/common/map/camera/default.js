import { usePreference } from '@/common/utils/preferences';
import { ref, watchEffect } from 'vue';
import { useStore } from 'vuex';
import { map } from '@/components/map/MapMain.vue';
import maplibregl from 'maplibre-gl';

const DefaultCamera = () => {
  // Datas uses
  const store = useStore();
  const initialized = ref(false);
  const defaultZoom = usePreference('zoom', 0);
  const defaultLatitude = usePreference('latitude');
  const defaultLongitude = usePreference('longitude');
  const selectedDeviceId = store.getters.deviceSelectedId;
  const positionsDevices = store.getters.positions;
  // Function for center map if have device or devices
  watchEffect(() => {
    // valida if you have selected device
    if (selectedDeviceId) initialized.value = true;
    else if (!initialized.value) {
      // valid if you add latitude and longitude
      if (defaultLatitude && defaultLongitude) {
        map.jumpTo({
          center: [defaultLongitude, defaultLatitude],
          zoom: defaultZoom,
        });
        initialized.value = true;
      } else {
        // Call the all positions of the devices
        const coordinates = Object.values(positionsDevices).map((item) => [item.longitude, item.latitude]);
        // if you use many devices
        if (coordinates.length > 1) {
          // funcion for center map with many positions
          const bounds = coordinates.reduce((bounds, item) => bounds.extend(item), new maplibregl.LngLatBounds(coordinates[0], coordinates[1]));
          const canvas = map.getCanvas();
          map.fitBounds(bounds, {
            duration: 0,
            padding: Math.min(canvas.width, canvas.height) * 0.1,
          });
          initialized.value = true;
        } else if (coordinates.length) {
          // if you only use one device
          const [individual] = coordinates;
          map.jumpTo({
            center: individual,
            zoom: Math.max(map.getZoom(), 10),
          });
          initialized.value = true;
        }
      }
    }
  });
};

export default DefaultCamera;
