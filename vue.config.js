/* eslint-disable import/no-extraneous-dependencies */

const { defineConfig } = require('@vue/cli-service');
const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = defineConfig({
  publicPath: '/modern',
  transpileDependencies: [
    'quasar',
  ],
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false,
    },
  },
  devServer: {
    onBeforeSetupMiddleware: (devServer) => {
      devServer.app.use(createProxyMiddleware('/api/socket', { target: `ws://${process.env.VUE_APP_URL_NAME}`, changeOrigin: true, ws: true }));
      devServer.app.use(createProxyMiddleware('/api', { target: `http://${process.env.VUE_APP_URL_NAME}` }));
    },
  },
});
